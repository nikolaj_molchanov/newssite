<?php
$title = $news->clearStr($_POST['title']);
$category = $news->clearInt($_POST['category']);
$description = $news->clearStr($_POST['description']);
$source = $news->clearStr($_POST['source']);

if ($title and $category and $description and $source) {

	if ($news->saveNews($title, $category, $description, $source)) {
		header('Location: news.php');
		exit;

	} else {
		$errMsg = 'Произошла ошибка при добавлении новостей!';
	}

} else {
	$errMsg = 'Заполните все поля формы!';
}
?>