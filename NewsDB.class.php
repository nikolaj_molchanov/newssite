<?php
function __autoload($name)
{
	if (is_file($name.'.class.php'))
		include $name.'.class.php';
}

class NewsDB implements INewsDB
{
	const DB_NAME = 'c:\OpenServer\domains\mysite.local\news.db';
	const RSS_NAME = 'rss.xml';
	const RSS_TITLE = 'Последние новости';
	const RSS_LINK = 'http://mysite.local/news/news.php';

	protected $_db;

	/**
	 * Контструктор
	 * Создаем БД и наполняем ее данными
	 */
	function __construct()
	{
		// Если БД существует, подключаемся
		if (file_exists(self::DB_NAME)) {
			$this->_db = new SQLite3(self::DB_NAME);

		// Иначе создаем и заполняем
		} else {
			$this->_db = new SQLite3(self::DB_NAME);

			$sql = 'CREATE TABLE msgs(
								id INTEGER PRIMARY KEY AUTOINCREMENT,
								title TEXT,
								category INTEGER,
								description TEXT,
								source TEXT,
								datetime INTEGER)';

			$this->_db->exec($sql) or die($this->_db->lastErrorMsg());

			$sql = 'CREATE TABLE category(
								id INTEGER,
								name TEXT)';

			$this->_db->exec($sql) or die($this->_db->lastErrorMsg());

			$sql = "INSERT INTO category(id, name)
								SELECT 1 as id, 'Политика' as name
								UNION SELECT 2 as id, 'Культура' as name
								UNION SELECT 3 as id, 'Спорт' as name";

			$this->_db->exec($sql) or die($this->_db->lastErrorMsg());
		}
	}

	/**
	 * Деструктор, закрываем соединение с БД
	 */
	function __destruct()
	{
		unset($this->_db);
	}

	/**
	 * Очистка полученных данных строк
	 *
	 * @param $data - Полученные данные
	 * @return mixed - результат обработанных данных
	 */
	function clearStr($data)
	{
		return $this->_db->escapeString(trim(strip_tags($data)));
	}

	/**
	 * Очистка полученных данных чисел
	 *
	 * @param $data - Полученные данные
	 * @return mixed - результат обработанных данных
	 */
	function clearInt($data)
	{
		return abs((int)$data);
	}

	/**
	 * Сохранение новостей
	 *
	 * @param string $title - Заголовок
	 * @param string $category - Название категории
	 * @param string $description - Описание
	 * @param string $source - Источник
	 * @return bool - Результат записи в БД
	 */
	function saveNews($title, $category, $description, $source)
	{
		try{
			$dt = time();

			$sql = 'INSERT INTO msgs (title, category, description, source, datetime) 
							VALUES (?,?,?,?,?)';

			$stmt = $this->_db->prepare($sql);

			$stmt ->bindParam(1, $title);
			$stmt ->bindParam(2, $category);
			$stmt ->bindParam(3, $description);
			$stmt ->bindParam(4, $source);
			$stmt ->bindParam(5, $dt, SQLITE3_INTEGER);

			$res = $stmt->execute();

			$stmt->close();

			if(!$res)
				throw new Exception($this->_db->lastErrorMsg());

			$this->createRss();

			$result = true;

		} catch(Exception $e) {
			$result = false;
		}

		return $result;
	}

	/**
	 * Получаем новости из БД
	 *
	 * @return array|bool - Результат выборки, или ошибка
	 */
	function getNews()
	{
		try{
			$arr_news = array();

			$sql = 'SELECT msgs.id as id,title,
							category.name as category, 
							description,
							source,
							datetime
						FROM msgs,
						category
						WHERE category.id = msgs.category
						ORDER BY msgs.id DESC';

			$result = $this->_db->query($sql);

			if(!is_object($result))
				throw new Exception($this->_db->lastErrorMsg());

			while($arr_new = $result->fetchArray(SQLITE3_ASSOC)){
				$arr_news[]= $arr_new;
			}

			$result = $arr_news;

		} catch(Exception $e) {
			$result = false;
		}

		return $result;
	}

	/**
	 * Удаление определенной новости
	 *
	 * @param int $id - Идентификатор новости
	 * @return bool - Результат удаления новости из БД
	 */
	function deleteNews($id)
	{
		try{

			$sql = "DELETE FROM msgs WHERE id=$id";
			$res = $this->_db->exec($sql);

			if(!$res)
				throw new Exception($this->_db->lastErrorMsg());

			$result = true;

		} catch(Exception $e) {
			$result = false;
		}

		return $result;
	}

	/**
	 * Создание Rss ленты
	 *
	 * @return bool
	 */
	function createRss(){

		$dom = new DOMDocument('1.0', 'utf-8');

		$dom->formatOutput = true;
		$dom->preserveWhiteSpace = false;

		$rss = $dom->createElement('rss');

		$dom->appendChild($rss);

		$version = $dom->createAttribute('version');
		$version->value='2.0';

		$rss->appendChild($version);

		$channel= $dom->createElement('channel');
		$rss->appendChild($channel);

		$title= $dom->createElement('title', self::RSS_TITLE);

		$channel->appendchild($title);

		$link= $dom->createElement('link', self::RSS_LINK);
		$channel->appendchild($link);

		$arr_news = $this->getNews();

		if(!is_array($arr_news)) return false;

		foreach ($arr_news as $arr_new) {
			$item = $dom->createElement('item');
			$title = $dom->createElement('title', $arr_new['title']);
			$link = $dom->createElement('link', $arr_new['source']);
			$description = $dom->createElement('description', $arr_new['description']);
			$pubDate = $dom->createElement('pubDate', date('d-m-Y H:i:s', $arr_new['datetime']));
			$category = $dom->createElement('category', $arr_new['category']);
			$item->appendChild($title);
			$item->appendChild($link);
			$item->appendChild($description);
			$item->appendChild($pubDate);
			$item->appendChild($category);
			$channel->appendChild($item);
		}

		$dom->save(self::RSS_NAME);
	}
}
?>