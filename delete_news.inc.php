<?php 

$id = $news->clearInt($_GET['del']);

if ($id) {
	if ($news->deleteNews($id)) {
		header('Location: news.php');
		exit;

	} else {
		$errMsg = 'Произошла ошибка при удалении новости!';
	}
}
?>